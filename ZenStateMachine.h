/*
Copyright (C) 2015 Hyundam Jeong

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any
damages arising from the use of this software.

Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and
redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must
not claim that you wrote the original software. If you use this
software in a product, an acknowledgment in the product documentation
would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and
must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source
distribution.

Hyundam Jeong
hdjeong@gmail.com

*/



#ifndef ZENSTATEMACHINE_H
#define ZENSTATEMACHINE_H

#include <sys/types.h>
#include <map>
#include <deque>
#include <string>
#include "ZMutex.h"

class ZState;

typedef enum {
	ZRESULT_SUCCESS,
	ZRESULT_INVALID_INPUT,
	ZRESULT_INVALID_OBJECT,
	ZRESULT_INVALID_TRANSITION,
	ZRESULT_REENTER_STATE,
	ZRESULT_UNDEFINED_STATE,
	ZRESULT_STATE_REDEFINED,
	ZRESULT_ERROR_OPENING_FILE,
	ZRESULT_UNKNOWN
} zresult_t;

class ZStateMachine
{
private:
	ZMutex					*m_pMutex;
	std::string				m_strName;
	std::string				m_strPrevState;
	std::string				m_strCurrState;
	ZState					*m_pStateHandler;
	std::deque<std::string>	m_History;
	std::map<std::string, ZState *>		m_StateMap;
	bool					m_bApplyTransitionGuard;
	u_int32_t				m_numState;
public:
	ZStateMachine(void);
	ZStateMachine(std::string name);
	virtual ~ZStateMachine(void);
	unsigned handle_event(void *pParam);
	std::string get_curr_state() { return m_strCurrState; }
	std::string get_prev_state() { return m_strPrevState; }
	void set_name(std::string name) { m_strName = name; }
	std::string get_name() { return m_strName; }
	zresult_t change_state(const std::string strNextState);
	zresult_t add_state(ZState *pState);
	zresult_t remove_state(ZState *pState, bool bDeleteState = true);
};

#endif // ZENSTATEMACHINE_H
