/*
Copyright (C) 2015 Hyundam Jeong

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any
damages arising from the use of this software.

Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and
redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must
not claim that you wrote the original software. If you use this
software in a product, an acknowledgment in the product documentation
would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and
must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source
distribution.

Hyundam Jeong
hdjeong@gmail.com

*/


#include <stdio.h>
#include <sys/types.h>
#include <sys/time.h>
#include "ZenStateMachine.h"
#include "ZenState.h"
#include "ZScopeLock.h"


ZStateMachine::ZStateMachine(void)
{
	ZStateMachine("");
}

ZStateMachine::ZStateMachine(std::string name)
{
	m_strName = name;
	m_strPrevState = "";
	m_strCurrState = "";
	m_bApplyTransitionGuard = false;
	m_numState = 0;
	m_pStateHandler = NULL;

	m_pMutex = new ZMutex();
}


ZStateMachine::~ZStateMachine(void)
{
	ZState *pState;
	if (m_StateMap.size() > 0)
	{
		std::map<std::string, ZState*>::iterator itor = m_StateMap.begin();
		while(itor != m_StateMap.end())
		{
			pState = itor->second;
			delete pState;
			++itor;
		}
	}
	
	m_StateMap.clear();
	
	if (m_pMutex)
		delete m_pMutex;
}

unsigned ZStateMachine::handle_event(void *pParam)
{
	if (m_pStateHandler != NULL)
		return m_pStateHandler->handle_event(pParam);

	return 0;
}

zresult_t ZStateMachine::change_state(const std::string nextState)
{
	ZScopeLock sl(m_pMutex);

	std::map<std::string, ZState*>::iterator itor = m_StateMap.find(nextState);

	if( itor == m_StateMap.end() )
		return ZRESULT_UNDEFINED_STATE;

	if (m_StateMap[nextState]->get_reenterable() == false && nextState.compare(m_strCurrState) == 0)
		return ZRESULT_INVALID_TRANSITION;
	
	if (m_pStateHandler != NULL)
		m_pStateHandler->exit(nextState);

	m_strPrevState = m_strCurrState;
	m_strCurrState = nextState;
	m_pStateHandler = m_StateMap[nextState];
	m_pStateHandler->enter(m_strPrevState);
	return ZRESULT_SUCCESS;
}

zresult_t ZStateMachine::add_state(ZState *pNewState)
{
	ZScopeLock sl(m_pMutex);

	if (!pNewState)
		return ZRESULT_INVALID_INPUT;

	std::string strState = pNewState->get_state_name();

	if (strState.compare("") == 0)
		return ZRESULT_INVALID_INPUT;

	std::map<std::string, ZState*>::iterator itor = m_StateMap.find(strState);

	if( itor != m_StateMap.end() )
		return ZRESULT_STATE_REDEFINED;

	m_StateMap[strState] = pNewState;
	pNewState->set_state_machine(this);
	
	return ZRESULT_SUCCESS;
}

zresult_t ZStateMachine::remove_state(ZState *pState, bool bDeleteState)
{
	ZScopeLock sl(m_pMutex);

	if (!pState)
	{
		return ZRESULT_INVALID_INPUT;
	}

	std::string strState = pState->get_state_name();

	if (strState.compare("") == 0)
	{
		return ZRESULT_INVALID_INPUT;
	}

	std::map<std::string, ZState*>::iterator itor = m_StateMap.find(strState);

	if( itor == m_StateMap.end() )
	{
		return ZRESULT_UNDEFINED_STATE;
	}

	ZState *pTargetState = m_StateMap[strState];
	if (bDeleteState)
		delete pTargetState;

	m_StateMap.erase(itor);

	return ZRESULT_SUCCESS;

}

