/*
Copyright (C) 2015 Hyundam Jeong

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any
damages arising from the use of this software.

Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and
redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must
not claim that you wrote the original software. If you use this
software in a product, an acknowledgment in the product documentation
would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and
must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source
distribution.

Hyundam Jeong
hdjeong@gmail.com

*/

#ifndef ZSCOPELOCK_H
#define ZSCOPELOCK_H

#include <pthread.h>
#include "ZMutex.h"

class ZScopeLock
{
private:
	ZMutex *m_pMutex;
	ZScopeLock() {}

public:
	ZScopeLock(ZMutex &rCS)
	{
		m_pMutex = &rCS;
		m_pMutex->Lock();
	}
	
	ZScopeLock(ZMutex *pCS)
	{
		m_pMutex = pCS;
		m_pMutex->Lock();
	}

	~ZScopeLock()
	{
		if (m_pMutex)
			m_pMutex->Unlock();

	}
};

#endif // ZSCOPELOCK_H
