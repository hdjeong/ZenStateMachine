/*
Copyright (C) 2015 Hyundam Jeong

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any
damages arising from the use of this software.

Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and
redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must
not claim that you wrote the original software. If you use this
software in a product, an acknowledgment in the product documentation
would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and
must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source
distribution.

Hyundam Jeong
hdjeong@gmail.com

*/

#ifndef ZENSTATE_H
#define ZENSTATE_H

#include <sys/types.h>
#include <string.h>
#include "ZenStateMachine.h"

class ZStateMachine;

typedef bool (*PFNENTER)(const std::string strPrevState);
typedef bool (*PFNHANDLEEVENT)(void *pParam);
typedef bool (*PFNEXIT)(const std::string strNextState);

class ZState
{
private:
	ZStateMachine *m_pSM;
	std::string m_strName;
	bool m_bReenterable;
	PFNENTER pfnEnter;
	PFNHANDLEEVENT pfnHandleEvent;
	PFNEXIT pfnExit;

	ZState();

protected:
	bool change_state(const std::string strNewState) { return m_pSM != NULL ? m_pSM->change_state(strNewState) : false; }

public:
	void set_state_machine(ZStateMachine *pSM) { m_pSM = pSM; }
	ZStateMachine *get_state_machine() { return m_pSM; }
	void set_state_name(const std::string strName) { m_strName = strName; }
	void set_reenterable(const bool bReenterable) { m_bReenterable = bReenterable; }
	bool get_reenterable(void) { return m_bReenterable; }
	std::string get_state_name() { return m_strName; }
	virtual bool enter(const std::string strPrevState) { return pfnEnter != NULL ? pfnEnter(strPrevState) : true; }
	virtual bool handle_event(void *pParam) { return pfnHandleEvent != NULL ? pfnHandleEvent(pParam) : true; }
	virtual bool exit(const std::string strNextState) { return pfnExit != NULL ? pfnExit(strNextState) : true; }

public:
	ZState(const std::string strName) { m_pSM = NULL; m_bReenterable = false; set_state_name(strName); pfnEnter = NULL; pfnHandleEvent = NULL; pfnExit = NULL; }
	virtual ~ZState() {}
	bool operator == (const ZState & a)
	{
		return    ( this->m_strName.length() == a.m_strName.length() )
			&& ( strcmp(this->m_strName.c_str(), a.m_strName.c_str()) == 0 );
	}
	bool bind(PFNENTER pfn_enter, PFNHANDLEEVENT pfn_handleevent, PFNEXIT pfn_exit) { pfnEnter = pfn_enter; pfnHandleEvent = pfn_handleevent; pfnExit = pfn_exit; }
};

#endif // ZENSTATE_H
