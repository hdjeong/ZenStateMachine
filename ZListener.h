/*
Copyright (C) 2015 Hyundam Jeong

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any
damages arising from the use of this software.

Permission is granted to anyone to use this software for any
purpose, including commercial applications, and to alter it and
redistribute it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must
not claim that you wrote the original software. If you use this
software in a product, an acknowledgment in the product documentation
would be appreciated but is not required.

2. Altered source versions must be plainly marked as such, and
must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any source
distribution.

Hyundam Jeong
hdjeong@gmail.com

*/

#ifndef ZLISTENER_H
#define ZLISTENER_H
#include <set>
#include <algorithm>

class ZListener {
public:
	virtual ~ZListener() {}
	virtual void notify(void *pParam) = 0;
};

class ZNotifier
{
private:
	std::set<ZListener*> listeners;
public:
	~ZNotifier()
	{
		std::set<ZListener*>::iterator it;
		for (it = listeners.begin(); it != listeners.end(); ++it) {
			delete (*it);
		}
		listeners.clear();
	}
	void add_listener(ZListener *l) { listeners.insert(l); }
	void remove_listener(ZListener *l) { listeners.erase(l); }
	void notify_listeners(void *pParam)
	{
		std::set<ZListener*>::iterator it;
		for (it = listeners.begin(); it != listeners.end(); ++it) {
			(*it)->notify(pParam);
		}
	}
	
};


#endif // ZLISTENER_H
